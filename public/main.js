var botID = location.hash.substr(1) || 'eggbot'

var clearButton = document.querySelector('#clear')
var sendButton = document.querySelector('#send')
var eggvid = document.querySelector('#eggvid')

var socket = io();
socket.on('connect', function(msg){
    console.log('socket.io connected!')
})
socket.on('hello',function(msg){
    console.log(msg)
})
socket.on('clientMessage',function(data){
    if(data.videoURL!=undefined){
        eggvid.src = data.videoURL
        eggvid.play()
    }else{
        alert(data.message)
    }
    console.log(data)
})




var canvas = document.getElementById('myCanvas')
var path

function onMouseDown(event) {
	path = new Path({
		strokeColor: 'black',
	})
}
function onMouseDrag(event) {
	path.add(event.point);
}
function onMouseUp(event) {
	path.simplify(10);
}



clearButton.addEventListener('click', function(e){
    project.clear()
})

sendButton.addEventListener('click', function(e){
    var wBase = 3200
    var hBase = 800
    var w = canvas.clientWidth
    var h = canvas.clientHeight
    var wScale = wBase/w
    var hScale = hBase/h
    var svg = project.exportSVG({
        asString: true,
        bounds: new Rectangle(0, 0, wBase, hBase),
        matrix:paper.view.matrix.clone().scale(wScale,hScale)
    })
    console.log(svg)
    socket.emit('sendToBot',{botID:botID, svg:svg})
})

// -----
// SimpleWebRTC stuff
var webrtc = new SimpleWebRTC({
    // the id/element dom element that will hold remote videos
    remoteVideosEl: 'remoteVideos',
    // immediately ask for camera access
    autoRequestMedia: false,
    localVideo: {
        autoplay:true,
        mirror:false
    }
})
webrtc.joinRoom('pceggcam2018')
webrtc.on('videoAdded', function(videoEl, peer){
    console.log('videoAdded')
    videoEl.setAttribute('playsinline', '')
    videoEl.setAttribute('muted', '')
})
webrtc.on('videoRemoved', function(videoEl){
    console.log('videoRemoved')
})
// -----