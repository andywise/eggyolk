// BOT
// Run this on the machine connected to the bot

var fs          = require('fs'),
    path        = require('path'),
    cmd         = require('node-command-line'),
    Promise     = require('bluebird'),
    socket      = require('socket.io-client')('http://eggyolk.herokuapp.com/'),
    cloudinary  = require('cloudinary')

    require('dotenv').config()

    // create a file named `.env` in the bot folder that contains the process.env variable values
    cloudinary.config({
        cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET
    })

    appDir  = path.dirname(require.main.filename),
    lastClientID = '',
    BOT_ID = process.env.BOT_ID || process.argv[2] || 'eggbot'// grab name from first command line param, or 'eggbot'
    console.log('bot ID: '+BOT_ID)

socket.on('connect', function(){
    console.log('connected!')
    socket.emit('registerBot', BOT_ID)
})
socket.on('botMessage',function(data){
    console.log(data)
    console.log('fill:',data.fill)
    lastClientID = data.clientID
    console.log('clientID',lastClientID)
    if(data.svg!=undefined){
        var inkscapeAppCmd = '/Applications/Inkscape.app/Contents/Resources/bin/inkscape '
            inkscapeAppCmd += '--verb=EditSelectAllInAllLayers --verb=ObjectToPath '// select all & convert text objects to paths
            if(data.fill == true){
                inkscapeAppCmd += '--verb command.eggbot.contributed.eggbot_hatch.revb2.noprefs '// hatch fill
            }
            inkscapeAppCmd += '--verb command.evilmadscientist.eggbot.rev240.noprefs ' // eggbot draw
            inkscapeAppCmd += '--verb=FileSave --verb=FileQuit  '// save and quit (to prevent hanging 'are you sure' dialog)
        var svgURI = path.join(appDir, '/', 'draw.svg')
        console.log(svgURI)
        fs.writeFile(svgURI, data.svg, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
            recordVideo()
            var commandToRun = `${inkscapeAppCmd} '${svgURI}'`
            runSingleCommandWithWait(commandToRun)
        })
        
    }
})

function runSingleCommandWithWait(command) {
    Promise.coroutine(function *() {
        yield cmd.run(command);
        console.log('Executed your command :)');
        socket.emit('botToClient',{
            clientID: lastClientID,
            message: 'Done drawing!'
        })
        stopRecordingVideo()
    })();
}



//
//
// VIDEO CAPTURE!
// requires videosnap and ffmpeg to be installed!

const { spawn, exec } = require('child_process')

console.log('camera name: '+process.env.CAMERA_NAME)

var cam = process.env.CAMERA_NAME || '0'// find your camera name in the terminal with `videosnap -l`

var app = "videosnap"
var args = ['-w',5,'-d',cam,'--no-audio', 'eggbot.mp4']
var dest = 'recordings/'
var child, unique


function recordVideo(){
    console.log('recordVideo...')
    child = spawn(app, args)
}
function stopRecordingVideo(){
    process.kill(child.pid, 'SIGINT');
    setTimeout(createFastVideo,1000)
}
function createFastVideo(){
    console.log('creating fast version now...')
    unique = new Date().valueOf()
    var thecmd = 'ffmpeg -i eggbot.mp4 -filter:v "setpts=0.2*PTS" "'+dest+unique+'.mp4"'
    console.log(thecmd)
    var speedup = exec(thecmd,{},()=>{
        console.log('video done!')

        // wait 1 second and create a gif of the video
        setTimeout(createFastGif,1000)
    })
}
function createFastGif(){
    // upload video to cloudinary
    var fileToUpload = dest + unique + '.mp4'
    console.log('fileToUpload:',fileToUpload)
    cloudinary.v2.uploader.upload(fileToUpload, 
        {
            resource_type: "video",
            upload_preset: "eggbot"
        },
        function(error, result) {
            if(error){
                console.log('error:',error)
            }
            console.log(result)
            socket.emit('botToClient',{
                clientID: lastClientID,
                videoURL: result.url
            })
    });

    console.log('creating fast GIF now...')
    var input = '"' + dest + unique + '.mp4"'
    var output = '"' + dest + unique + '.gif"'
    var thecmd = 'ffmpeg -i ' + input + ' -r "8" ' + output
    console.log(thecmd)
    var fastgif = exec(thecmd,{},()=>{
        console.log('gif done!')
    })
}