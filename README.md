# eggyo.lk

## Features
Eggyo.lk provides a web-based interface that allows a human to draw an image on an egg with a remotely connected eggbot. It consists of:

* **an eggbot host app** that receives drawings from the remote drawing app (using [socket.io-client](https://github.com/socketio/socket.io-client))
* **a node.js relay server** to pass messages between the drawing app and the eggbot
* **a simple web-based drawing app client** (using [paper.js](http://paperjs.org))

`[drawing app "client"] <==> [node relay server] <==> [eggbot "host" computer]`

## Eggbot Setup
Install [Inkscape](https://inkscape.org/en/release/0.91/?latest=1) (recommended version 0.91) and [Eggbot Extension](https://github.com/evil-mad/EggBot/releases/tag/v2.4.0) (recommended version 2.4.0) on the "host" computer to which the eggbot is connected.

Manually install the Eggbot Extension files in the `~/.config/inkscape/extensions/` directory.

## Prerequisites
This setup requires homebrew, ffmpeg, videosnap, and node to be installed.

**Install command line tools**  
`xcode-select --install`

**Install homebrew**  
`ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

**Install ffmpeg**  
`brew install ffmpeg --with-fdk-aac --with-ffplay --with-freetype --with-frei0r --with-libass --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-rtmpdump --with-schroedinger --with-speex --with-theora --with-tools`

**Install videosnap**

If you already have **wget** installed:
`wget https://github.com/matthutchinson/videosnap/blob/master/release/videosnap-0.0.3.tar.gz\?raw\=true -O videosnap.tar.gz`  
`tar -zxvf videosnap.tar.gz`  
`cp -R videosnap/usr/local/* /usr/local/`

If you don't have **wget** installed, download **videosnap** here, and follow the installation instructions:  
[https://github.com/matthutchinson/videosnap](https://github.com/matthutchinson/videosnap)


**Install node**  
`brew install node`

## How to use
1. **Host Computer**
	- Connect the eggbot to the computer.
	- Place the contents of the `/bot` directory somewhere on the same "host" computer.
	- For video recording, run `videosnap -l` in terminal to list available cameras, then update the `CAMERA_NAME` cariable in the `example.env` file.
	- For Cloudinary uploading, update your Cloudinary API credentials in the `example.env` file (`CLOUDINARY_CLOUD_NAME`, `CLOUDINARY_API_KEY`, `CLOUDINARY_API_SECRET`).
	- If you plan to operate multiple eggbots, update the `BOT_ID` variable in the `example.env` file. (i.e. `eggbot1`, `eggbot2`, etc.)
	- rename `example.env` to `.env`
	- `cd bot`
	- `npm install`
	- `node bot.js` to start eggbot host app.
2. **Remote Server** (i.e. Heroku)
	- Place all other files from the root of this project on a remote server.
	- `npm install`
	- `node index.js`
3. **Drawing Client**
	- Open `http://www.remoteserver.com/draw` in a browser (i.e. [eggyolk.herokuapp.com/draw](http://eggyolk.herokuapp.com/draw)).
	- To target a specific eggbot if you are running multiple bots, add `#yourbotID` to the URL to match the `BOT_ID` in your bot's `.env` file (i.e. [eggyolk.herokuapp.com/draw#eggbot2](eggyolk.herokuapp.com/draw#eggbot2))
	- Draw something.
	- Click the `Send to Eggbot!` button.
	- When the eggbot is done drawing, you'll receive an alert in your browser, and a timelapse video of the eggbot should autoplay.