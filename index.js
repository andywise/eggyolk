// RELAY SERVER
// Run this on cloud server to relay socket.io websockets messages from client(s) to bot(s)

console.log('running index.js...')

// MODULES
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    compression = require('compression'),

    NounProject = require('the-noun-project'),
    nounProject = new NounProject({
        key: process.env.NOUNPROJECT_KEY,
        secret: process.env.NOUNPROJECT_SECRET
    });

// VARS
const PORT = process.env.PORT || 3000
var botSocketIDs = {}

// EXPRESS SETUP
// allow cors per https://stackoverflow.com/questions/43915577/nodejs-express-heroku-cors
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
    next();
});
app.use(compression());// GZIP all assets per https://www.sitepoint.com/5-easy-performance-tweaks-node-js-express/
app.use(express.static('public'));// allow static files in public directory

// ROUTES
app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});
app.get('/draw', function(req, res){
    res.sendFile(__dirname + '/public/draw.html');
});
app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
});

// noun project api search!
app.get('/np/:searchterm', function (req, res) {
    var search = [req.params.searchterm][0]
    console.log('search: ',search)
    if(search){
        nounProject.getIconsByTerm(search, {
            limit_to_public_domain: true,
            limit: 50
        }, function (err, data) {
            if(err){
                console.log('error:',err)
                res.send(err)
                return
            }
            res.send(data)
        })
    }else{
        res.send('No search term!')
    }
    
})

// SOCKET STUFF
io.on('connection', function(socket){

    console.log('user connected')
    socket.emit('hello','Hi there.')

    socket.on('registerBot', function(data){
        console.log('registering a bot...',data)
        botSocketIDs[data] = socket.id
        io.emit('botConnect', {botID: data})
    })

    function relayTo(type, eventID, data){
        var targetSocket = (type=='botID') ? botSocketIDs[data[type]] : data[type]
        if (data!=undefined && data.hasOwnProperty(type)) io.to(targetSocket).emit(eventID, data)
    }
    socket.on('sendToBot', function(data){
        data.clientID = socket.id
        relayTo('botID', 'botMessage', data)
    })
    socket.on('botToClient', function(data){
        relayTo('clientID', 'clientMessage', data)
    })

    socket.on('disconnect', function(){
        var vals = Object.keys(botSocketIDs).map(function (key) {
            return botSocketIDs[key]
        });
        var bi = vals.indexOf(socket.id)
        if(bi>=0){
            var botID = Object.keys(botSocketIDs)[bi]
            console.log(`botID: ${botID} disconnected.`)
            io.emit('botDisconnect', {botID: botID})
            delete botSocketIDs[botID]
            return
        }else{
            console.log(`Non-bot disconnected: ${socket.id}`)
        }
    })
});// end socket.on('connection'


// START LISTENING!
http.listen(PORT, () => console.log(`Listening on ${ PORT }`));